# Prepackaged Qi Integration

Integration scripts for using prepackaged open source cell segmentation algorithms within QiTissue.

Will help download and execute the algorithms

We are wrapping the following algorithms to be able to easily install the dependencies

- [Cellpose](https://gitlab.com/qimagingsys/qi-cellpose)
- [Stardist](https://gitlab.com/qimagingsys/qi-stardist)
- [DeepCell](https://gitlab.com/qimagingsys/qi-deepcell) (Experimental)

## Documentation:

- **[Usage](docs/usage.md)**
- **[Development](docs/releases.md)**

## Current State of the Integration

|                                             | Stardist | Cellpose      | DeepCell |
|---------------------------------------------|----------|---------------|----------|
| pip Package                                 | ✅        | ✅             | ✅        |
| Running in QiTissue                         | ✅        | ✅             | ✅        |
|                                             |          |               |          |
| Additional Options implemented              | ☐ [^5]   | ✅             | ☐        |
| using Custom Model implemented              | ✅        | ✅             | ☐        |
|                                             |          |               |          |
| Cytoplasm + Nuclear Seg.                    | [~] [^2] | ☐ [^3]        | ✅ [^4]   |
|                                             |          |               |          |
| Windows                                     | ✅        | ✅             | ✅        |
| Mac (x86)                                   | ✅        | ✅             | ☐ [^6]   |
| Mac (Apple Silicon)                         | ✅        | ✅             | ☐ [^1]   |
| Linux [^7]                                  | ✅       | ✅            | ✅       |
|                                             |          |               |          |
| GPU Support Tested                          | ✅        | ✅             | ✅        |
| GPU Support Implemented (Windows)           | ✅ [^9]   | ✅ [^8]        | ✅ [^9]   |
| GPU Support Implemented (Mac x86)           | ☐  [^6]  | ☐ ([^8],[^6]) | ☐ [^6]   |
| GPU Support Implemented (Mac Apple Silicon) | ✅        | ✅             | ☐        |
| GPU Support Implemented (Linux)             | ☐  [^6]  | ☐ ([^8],[^6]) | ☐ [^6]   |
|                                             |          |               |          |
| Tested on large images                      | ☐        | ☐             | ☐        |
| Natively implemented in QiTissue            | ☐        | ☐             | ☐        |

[^1]: Tensorflow still problematic on Apple Silicon. Some tests succeeded with tensorflow 2.13 and Python 3.11
[^2]: Stardist models only supports nuclear segmentation
[^3]: has separate models for both, should be possible to combine the results but might not be completely accurate
[^4]: Model `mesmer-both` outputs two masks
[^5]: No direct options supported, but some base model options are
available [link](https://github.com/stardist/stardist/blob/468c60552c8c93403969078e51bddc9c2c702035/stardist/models/model2d.py#L109C2-L109C2)
[^6]: Not tested yet
[^7]: On Linux QiTissue is distributed as an AppImage. In order to install and run the prepackaged-qi-integration tools, the AppImage needs to be installed locally. Do so by downloading the AppImage and run:\
`./QiTissue-1.***-****.AppImage --appimage-extract`\
`mv squashfs-root ~/QiTissue`     (or wherever you like)\
`~/QiTissue/AppRun`      (or create a symbolic link to this file in your bin directory)\
This resolves previously noted issues: On linux the distutils package is missing from the embedded python. Also the path of the embedded python changes
because it is an AppImage, this breaks virtual environments
[^8]: Nvidia GPU and Cuda 11.8 required [link](https://developer.nvidia.com/cuda-11-8-0-download-archive)
[^9]: Nvidia GPU, [Cuda 11.2](https://developer.nvidia.com/cuda-11-2-0-download-archive)
and [CuDNN 8.1](https://developer.nvidia.com/cudnn) required. For CuDNN an Nvidia account and accepting a license is
required. Extract CuDNN into the CUDA 11.2 folder. If the error message 'You already have a newer version of the NVIDIA
Frameview SDK installed' appears, uninstall Frameview SDK first.  


