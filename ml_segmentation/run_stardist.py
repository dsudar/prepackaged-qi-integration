import logging

from ml_segmentation._ui.stardist import StardistWidget
from ml_segmentation._utils.certifi import setup_certificates_mac
from ml_segmentation._utils.cwd import change_to_module_cwd
from ml_segmentation._utils.logging import init_logger

init_logger()


def run():
    try:
        logging.debug('Changing working dir')
        change_to_module_cwd()
        logging.debug('Changed working dir')
        logging.debug('Certifi')
        setup_certificates_mac()
        logging.debug('Certifi finished')
        logging.debug('Building stardist ui')
        widget = StardistWidget()
        logging.debug('Finished building ui')
        logging.debug('Start running stardist')
        widget.exec_()
        logging.info('Finished running stardist')
    except Exception:
        logging.exception('Exception while running stardist')
        raise


run()
