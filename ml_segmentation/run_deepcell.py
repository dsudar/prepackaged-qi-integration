import os

from ml_segmentation._ui.deepcell import DeepcellWidget
from ml_segmentation._utils.certifi import setup_certificates_mac
from ml_segmentation._utils.cwd import change_to_module_cwd
import logging
from ml_segmentation._utils.logging import init_logger

import ml_segmentation._deepcell_token as dt

init_logger()

def run():
    try:
        os.environ.update({"DEEPCELL_ACCESS_TOKEN": dt.deepcell_token})
        logging.debug('Changing working dir')
        change_to_module_cwd()
        logging.debug('Changed working dir')
        logging.debug('Certifi')
        setup_certificates_mac()
        logging.debug('Certifi finished')
        logging.debug('Building deepcell ui')
        widget = DeepcellWidget()
        logging.debug('Finished deepcell ui')
        logging.debug('Start running deepcell')
        widget.exec_()
        logging.info('Finished running deepcell')
    except Exception:
        logging.exception('Exception while running deepcell')
        raise


run()
