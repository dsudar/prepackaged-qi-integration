from typing import Literal

from PySide2.QtWidgets import QFormLayout, QLineEdit, QPushButton, QFileDialog, QHBoxLayout

from ml_segmentation._ui.base import SegmentationWidget


class StardistWidget(SegmentationWidget):
    def __init__(self, callback=None, parent=None):
        super().__init__(parent, models=['2D_versatile_fluo', '2D_paper_dsb2018'], has_custom_model_option=True)

        self.setWindowTitle("Stardist Segmentation")

        # Connect the run button to the callback function
        if callback:
            self.callback = callback

    def add_custom_model_ui(self, layout: QFormLayout):
        self.model_folder_display = QLineEdit()
        self.model_folder_button = QPushButton("Select")

        line = QHBoxLayout()
        line.addWidget(self.model_folder_display)
        line.addWidget(self.model_folder_button)

        layout.addRow('Model Folder', line)

        self.model_folder_button.clicked.connect(self.select_folder)

    def select_folder(self):
        options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly
        folder = QFileDialog.getExistingDirectory(self, "Select Folder", "", options=options)
        if folder:
            self.model_folder_display.setText(folder)

    def activate_custom_model_option(self, activate: bool):
        self.model_folder_display.setEnabled(activate)
        self.model_folder_button.setEnabled(activate)

    def get_extra_cli_options(self) -> list[str]:
        if self.is_custom_model():
            return ['--model_folder', f'{self.model_folder_display.text()}']
        else:
            return []

    def get_algo_package(self) -> Literal['qi_stardist', 'qi_cellpose', 'qi_deepcell']:
        return 'qi_stardist'
