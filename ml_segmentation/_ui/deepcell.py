from typing import Literal

from PySide2.QtWidgets import QFormLayout, QHBoxLayout, QCheckBox, QDoubleSpinBox, \
    QLabel

from ml_segmentation._ui.base import SegmentationWidget


class DeepcellWidget(SegmentationWidget):
    def __init__(self, callback=None, parent=None):
        super().__init__(parent, models=['nuclear', 'mesmer-nuclear', 'mesmer-both'], has_custom_model_option=False)

        self.setWindowTitle("Deepcell Segmentation")

        # Connect the run button to the callback function
        if callback:
            self.callback = callback

    def add_extra_ui_options(self, layout: QFormLayout):
        line = QHBoxLayout()
        self.image_mpp_check = QCheckBox('active')
        self.image_mpp_value = QDoubleSpinBox()
        self.image_mpp_value.setRange(0, 100)
        self.image_mpp_value.setEnabled(False)
        line.addWidget(self.image_mpp_check)
        line.addWidget(self.image_mpp_value)

        def update_image_mpp_state():
            self.image_mpp_value.setEnabled(self.image_mpp_check.isChecked())

        self.image_mpp_check.toggled.connect(update_image_mpp_state)
        update_image_mpp_state()

        layout.addRow('image mpp', line)

    def get_extra_cli_options(self) -> list[str]:
        options = []
        if self.image_mpp_check.isChecked():
            options.extend(['-mpp', f'{self.image_mpp_value.value()}'])
        return options

    def get_algo_package(self) -> Literal['qi_stardist', 'qi_cellpose', 'qi_deepcell']:
        return 'qi_deepcell'

    def model_needs_cyto_channel(self) -> bool:
        # mesmer models
        return self.models[self.model_combobox.currentIndex()].startswith('mesmer')

    def model_has_cyto_output(self) -> bool:
        return self.models[self.model_combobox.currentIndex()] == 'mesmer-both'
