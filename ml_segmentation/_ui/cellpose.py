from typing import Literal

from PySide2.QtWidgets import QPushButton, QFileDialog, QFormLayout, QHBoxLayout, QLabel, QSpinBox, QCheckBox, \
    QDoubleSpinBox, QLineEdit

from ml_segmentation._ui.base import SegmentationWidget


class CellposeWidget(SegmentationWidget):

    def __init__(self, callback=None, parent=None):
        super().__init__(parent, models=['nuclei'], has_custom_model_option=True)
        # ['nuclei', 'cyto', 'cyto2']

        self.setWindowTitle("Cellpose Segmentation")

        # Connect the run button to the callback function
        if callback:
            self.callback = callback

    def add_extra_ui_options(self, layout: QFormLayout):
        self.net_avg_check = QCheckBox('NetAvg')
        self.net_avg_check.setToolTip('If selected Cellpose will take the average of 4 built-in pre-trained networks. (potentially better generalization)')
        layout.addRow('Net Average:', self.net_avg_check)

        line = QHBoxLayout()
        self.average_diameter_input = QSpinBox()
        self.average_diameter_input.setValue(20)
        self.average_diameter_input.setRange(0, 9999)
        self.auto_check = QCheckBox('Auto')
        self.auto_check.setChecked(True)
        line.addWidget(self.auto_check)
        line.addWidget(self.average_diameter_input)
        line.addWidget(QLabel('px'))

        def update_diameter_state():
            self.average_diameter_input.setEnabled(not self.auto_check.isChecked())

        self.auto_check.toggled.connect(update_diameter_state)
        update_diameter_state()

        layout.addRow('Average Cell Diameter:', line)

        self.flow_theshold = QDoubleSpinBox()
        self.flow_theshold.setValue(0.4)
        self.flow_theshold.setRange(0.0, 1.0)
        self.flow_theshold.setToolTip('flow error threshold (all cells with errors below threshold are kept)')
        layout.addRow('Flow Threshold:', self.flow_theshold)

        self.cellprob_threshold = QDoubleSpinBox()
        self.cellprob_threshold.setValue(0.0)
        self.cellprob_threshold.setRange(0.0, 1.0)
        self.cellprob_threshold.setToolTip('all pixels with value above threshold kept for masks, decrease to find more and larger masks')
        layout.addRow('Cell Prob. Threshold:', self.cellprob_threshold)
        
    def add_custom_model_ui(self, layout: QFormLayout):
        self.model_file_display = QLineEdit()
        self.model_file_button = QPushButton("Select")
        self.modelFile = ''

        line = QHBoxLayout()
        line.addWidget(self.model_file_display)
        line.addWidget(self.model_file_button)

        layout.addRow('Model File', line)

        self.model_file_button.clicked.connect(self.select_file)

    def select_file(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Select a cellpose model file")
        if file_name:
            self.model_file_display.setText(f"{file_name}")
            self.modelFile = file_name

    def activate_custom_model_option(self, activate: bool):
        self.model_file_display.setEnabled(activate)
        self.model_file_button.setEnabled(activate)
        if hasattr(self, 'auto_check'):
            if activate:
                # not an option for custom model
                self.auto_check.setChecked(False)
            self.auto_check.setEnabled(not activate)

    def get_extra_cli_options(self) -> list[str]:
        options = []

        if not self.auto_check.isChecked():
            options.extend(['-d', f'{self.average_diameter_input.value()}'])
        if self.net_avg_check.isChecked():
            options.extend(['--net_avg'])
        if self.is_custom_model() and self.modelFile:
            options.extend(['--model_file', self.modelFile])
            
        options.extend(['-f', str(self.flow_theshold.value())])
        options.extend(['-c', str(self.cellprob_threshold.value())])

        return options

    def get_algo_package(self) -> Literal['qi_stardist', 'qi_cellpose', 'qi_deepcell']:
        return 'qi_cellpose'
