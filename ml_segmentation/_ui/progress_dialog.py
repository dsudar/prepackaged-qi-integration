from PySide2.QtCore import Qt, QCoreApplication
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QProgressDialog


def show_progress_dialog(message: str = None):
    dialog = QProgressDialog()
    dialog.setWindowTitle("Please Wait")
    dialog.setWindowFlags(Qt.Dialog | Qt.WindowTitleHint | Qt.CustomizeWindowHint)

    font = QFont()
    font.setPointSize(12)
    dialog.setFont(font)

    dialog.setLabelText(message)
    dialog.setCancelButton(None)
    dialog.setRange(0, 0)  # Indefinite progress
    dialog.setModal(True)
    dialog.show()

    QCoreApplication.processEvents()

    return dialog
