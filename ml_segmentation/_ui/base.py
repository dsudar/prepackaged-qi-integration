import abc
import logging
from abc import abstractmethod, ABCMeta
from typing import Literal

from PySide2 import QtWidgets
from PyQiTissue import QiApi
from PySide2.QtWidgets import QFormLayout, QHBoxLayout, QSpinBox, QCheckBox

from ml_segmentation._segmentation.runner import run_segmentation


class SegmentationWidgetMeta(ABCMeta, type(QtWidgets.QDialog)):
    pass


class SegmentationWidget(abc.ABC, QtWidgets.QDialog, metaclass=SegmentationWidgetMeta):
    def __init__(self, parent=None, models: list[str] = None, has_custom_model_option=False):
        super().__init__(parent)

        self.setWindowTitle("Segmentation")
        self.layout = QtWidgets.QFormLayout()

        if models is not None:
            self.model_combobox = QtWidgets.QComboBox()
            if has_custom_model_option:
                models.append('Own Model')
            self.models = models
            self.model_combobox.addItems(models)
            self.layout.addRow("Model:", self.model_combobox)

        if has_custom_model_option:
            self.add_custom_model_ui(self.layout)
            self.activate_custom_model_option(False)

        self.model_combobox.currentIndexChanged.connect(self.on_model_changed)

        self.channel_combobox = QtWidgets.QComboBox()
        self.channel_combobox.addItems(QiApi.Ui.currentChannelNames())
        self.layout.addRow("Channel:", self.channel_combobox)

        self.cyto_channel_combobox = QtWidgets.QComboBox()
        self.cyto_channel_combobox.addItems(QiApi.Ui.currentChannelNames())
        self.cyto_channel_combobox.setEnabled(False)
        self.layout.addRow("Cyto Channel:", self.cyto_channel_combobox)

        self.rois = [annotation for annotation in QiApi.Annotation.listAnnotationsInCurrentView() if
                     QiApi.Annotation.role(annotation) == QiApi.Annotation.AnnotationRole.ROI]
        self.rois.insert(0, None)
        self.roi_combobox = QtWidgets.QComboBox()

        def roi_title(roi):
            if roi is None:
                return 'Full Image'
            elif roi == -1:
                return 'All ROIs'
            else:
                return QiApi.Annotation.title(roi)

        if len(self.rois) > 1:
            self.rois.insert(0, -1)
        self.roi_combobox.addItems([roi_title(roi) for roi in self.rois])
        self.layout.addRow("ROI:", self.roi_combobox)

        self.add_extra_ui_options(self.layout)

        self.cyto_combobox = QtWidgets.QComboBox()
        self.cyto_combobox.addItems(['None', 'Donut', 'Constrained Donut'])
        self.cyto_combobox.setCurrentIndex(1)
        self.layout.addRow('Cytoplasm Method', self.cyto_combobox)

        layout_donut_width = QHBoxLayout()
        self.donut_width_value = QSpinBox()
        self.donut_width_value.setValue(0)
        self.donut_width_value.setRange(0, 9999)
        self.donut_width_value.setEnabled(False)
        self.donut_width_check = QCheckBox("Active")

        def activate_donut_width():
            self.donut_width_value.setEnabled(self.donut_width_check.isChecked())

        self.donut_width_check.toggled.connect(activate_donut_width)
        layout_donut_width.addWidget(self.donut_width_check)
        layout_donut_width.addWidget(self.donut_width_value)
        self.layout.addRow("Donut width:", layout_donut_width)

        self.gpu_mode = QtWidgets.QComboBox()
        self.gpu_mode.addItems(['no', 'auto'])
        self.gpu_mode.setCurrentIndex(1)
        self.layout.addRow('gpu mode ', self.gpu_mode)

        self.run_button = QtWidgets.QPushButton("Run")
        self.run_button.clicked.connect(self.start)
        self.layout.addRow(self.run_button)

        self.progress_bar = QtWidgets.QProgressBar()
        self.progress_bar.setRange(0, 0)
        self.progress_bar.hide()
        self.layout.addRow(self.progress_bar)

        self.setLayout(self.layout)

    def add_custom_model_ui(self, layout: QFormLayout):
        pass

    def on_model_changed(self, index):
        if self.models[index] == 'Own Model':
            self.activate_custom_model_option(True)
        else:
            self.activate_custom_model_option(False)

        self.cyto_channel_combobox.setEnabled(self.model_needs_cyto_channel())
        self.cyto_combobox.setEnabled(not self.model_has_cyto_output())

    def model_needs_cyto_channel(self) -> bool:
        return False

    def model_has_cyto_output(self) -> bool:
        return False

    def is_custom_model(self) -> bool:
        return self.models[self.model_combobox.currentIndex()] == 'Own Model'

    def activate_custom_model_option(self, activate: bool):
        pass

    def add_extra_ui_options(self, layout: QFormLayout):
        pass

    def get_extra_cli_options(self) -> list[str]:
        return []

    def start(self):
        """
        Start the progress bar and invoke the callback function.
        """
        self.progress_bar.show()
        self.progress_bar.setRange(0, 1)

        self.run_segmentation()

        self.progress_bar.hide()

        self.close()

    def run_segmentation(self):
        logging.debug('Collect data for segmentation')
        cyto = ['None', 'donut', 'constrainedDonut'][self.cyto_combobox.currentIndex()]

        roi_option = self.rois[self.roi_combobox.currentIndex()]
        rois = [roi_option]
        if roi_option == -1:
            # All Rois
            rois = self.rois[2:]

        if len(rois) > 1:
            self.progress_bar.setRange(0, len(rois))

        counter = 0

        model = self.model_combobox.currentText()
        if model == 'Own Model':
            # custom model
            model = None

        cyto_channel_index = -1
        if self.model_needs_cyto_channel():
            cyto_channel_index = self.cyto_channel_combobox.currentIndex()

        logging.debug('Collected data')
        logging.debug('Starting segmentation on selected rois')
        for roi in rois:
            if roi is not None:
                print(f'Running Segmentation on {QiApi.Annotation.title(roi)}')
                logging.debug(f'Running segmentation on {QiApi.Annotation.title(roi)}')
                if len(rois) > 1:
                    self.progress_bar.setValue(counter)

            run_segmentation(
                package=self.get_algo_package(),
                model=model,
                channel_index=self.channel_combobox.currentIndex(),
                cyto_channel_index=cyto_channel_index,
                roi=roi,
                cyto=cyto,
                donut_width=self.donut_width_value.value(),
                gpu_mode=self.gpu_mode.currentText(),
                extra_options=self.get_extra_cli_options(),
                combined_output=self.model_has_cyto_output()
            )
            if roi is not None:
                logging.debug(f'Finished segmentation on {QiApi.Annotation.title(roi)}')
            else:
                logging.debug('Finished segmentation on picture')
            counter += 1
        logging.info('finished segmentation on selected rois')
        self.progress_bar.setValue(len(rois))

    @abstractmethod
    def get_algo_package(self) -> Literal['qi_stardist', 'qi_cellpose', 'qi_deepcell']:
        raise NotImplementedError
