import logging

from ml_segmentation._ui.progress_dialog import show_progress_dialog
from ml_segmentation._utils.cwd import change_to_module_cwd
from ml_segmentation._utils.install_in_venv import create_and_install_package
from ml_segmentation._utils.logging import init_logger

init_logger()


def _install():
    wait = show_progress_dialog('Installing Cellpose, this may take a while')
    logging.debug('Start installing qi_cellpose')

    try:
        logging.debug('Changing working dir')
        change_to_module_cwd()
        logging.debug('Changed working dir')
        create_and_install_package('qi_cellpose==3.1.3',
                                   [
                                       'https://gitlab.com/api/v4/groups/qimagingsys/-/packages/pypi/simple',
                                       # for cuda support
                                       'https://download.pytorch.org/whl/cu118'
                                   ])
        logging.info('Installation qi_cellpose finished')
    except Exception:
        logging.exception('Exception while installing qi_cellpose')
        raise
    wait.close()


_install()
