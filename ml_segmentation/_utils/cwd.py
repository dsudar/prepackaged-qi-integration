import os
import ml_segmentation


def change_to_module_cwd():
    # Change the working directory to the script's directory
    script_dir = os.path.dirname(os.path.abspath(ml_segmentation.__file__))
    os.chdir(script_dir)