import logging
import os
import pathlib
from typing import Union, List

from PySide2 import QtCore

from virtualenv import cli_run

from ml_segmentation._utils.command import run_command


def create_and_install_package(package_name: str, extra_index_url: Union[str, List[str]] = None) -> None:
    """
    Create a new virtual environment, install a package within it using pip,
    with an optional extra index URL.

    Args:
        package_name (str): The name of the package to install.
        extra_index_url (str, optional): The optional extra index URL. Defaults to None.
    """
    logging.debug('creating virtual env')
    venv_dir = get_venv_name(package_name)

    create_venv(venv_dir)
    logging.debug('activating virtual env')
    activate_virtual_environment(venv_dir)
    logging.info('created virtual env')
    logging.debug('installing packages')
    install_package(package_name, extra_index_url)
    logging.info(f'Package {package_name} installed in the virtual environment {os.path.abspath(venv_dir)}!')


def create_venv(venv_dir: str):
    cli_run(["-p", get_python_executable(), venv_dir], setup_logging=False)


def get_venv_name(package_name: str):
    venv_dir = package_name.lower().replace("-", "_")
    venv_dir = venv_dir.split('==')[0]
    venv_dir = venv_dir + '_venv'
    user_path = pathlib.Path.home() / 'Qi'
    user_path.mkdir(parents=True, exist_ok=True)
    venv_dir = user_path / venv_dir
    venv_dir.mkdir(parents=True, exist_ok=True)
    return venv_dir.as_posix()


def install_package(package: str, extra_index_url: Union[str, List[str]] = None) -> None:
    """
    Install a package using pip.

    Args:
        package (str): The name of the package to install.
        extra_index_url (str, optional): The optional extra index URL. Defaults to None.
    """
    pip_cmd = ['pip', 'install', package]
    if extra_index_url:
        if isinstance(extra_index_url, str):
            extra_index_url = [extra_index_url]  # Convert a single string to a list

        for url in extra_index_url:
            pip_cmd.extend(['--extra-index-url', url])

    pip_cmd.extend(['--progress-bar', 'off'])

    # potentially https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=powershell#enable-long-paths-in-windows-10-version-1607-and-later
    # long paths need to be enabled on windows
    run_command(pip_cmd)


def activate_virtual_environment(venv_dir: str) -> None:
    """
    Activate the virtual environment.

    Args:
        venv_dir (str): The path to the virtual environment directory.
    """
    activate_script = os.path.join(venv_dir, 'Scripts' if os.name == 'nt' else 'bin', 'activate_this.py')
    exec(open(activate_script).read(), {'__file__': activate_script})


# TODO: test on OS configurations
def get_python_executable() -> str:
    """
    Get the Python executable path based on the operating system.

    Returns:
        str: The Python executable path.
    """
    if os.name == 'nt':  # Windows
        python_executable = str(pathlib.Path(os.__file__).parents[1] / 'python.exe')
    elif os.name == 'posix':
        python_executable = str(pathlib.Path(os.__file__).parents[2] / 'bin' / 'python3' )   
    else:  # Unix-like
        python_executable = str(pathlib.Path(os.__file__).parents[2] / 'bin' / 'python3')
    
    return python_executable
