import subprocess
import sys
import io
from PySide2 import QtCore
import logging
import binascii

import platform

startupinfo = None
if platform.system() == 'Windows':
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    startupinfo.wShowWindow = subprocess.SW_HIDE


def run_command_live(command: list[str]):

    logging.debug(f'running command {" ".join(command)}')

    process = subprocess.Popen(command, bufsize=1, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=startupinfo)

    for line in iter(process.stdout.readline, ''):
        QtCore.QCoreApplication.processEvents()
        logging.info(line)
        process.stdout.flush()

    process.wait()
    #
    # while process.stdout.readable():
    #     print('------------------')
    #     line = process.stdout.readline()
    #     print(line)
    #
    #     QtCore.QCoreApplication.processEvents()
    #     print('------------------')
    #     sys.stdout.flush()
    #     print('------------------')
    #
    #     print('------------------')


def run_command_no_capture(command: list[str]):
    QtCore.QCoreApplication.processEvents()
    process = subprocess.Popen(command, startupinfo=startupinfo)

    process.wait()


def run_command(command):
    result = subprocess.run(command, universal_newlines=True, capture_output=True, startupinfo=startupinfo, encoding='utf-8')
    try:
        logging.info(result.stdout.encode())
        logging.error(result.stderr.encode())
    except:
        hex_output = binascii.hexlify(result.stdout).decode('utf-8')
        logging.info("hexed stdout:",hex_output,"  End of stdout")
        hex_output = binascii.hexlify(result.stderr).decode('utf-8')
        logging.info("hexed stderr:",hex_output,"  End of stderr")



def run_command_qprocess(command: list[str]):
    process = QtCore.QProcess()

    # def read_output():
    #     while process.canReadLine():
    #         output_line = process.readLine().data().decode().strip()
    #         if output_line:
    #             print(output_line)
    #
    # def read_error():
    #     error_output = process.readAllStandardError().data().decode()
    #     if error_output:
    #         print(f"Error:\n{error_output}")
    #         raise Exception(error_output)
    #
    # process.readyReadStandardOutput.connect(read_output)
    # process.readyReadStandardError.connect(read_error)

    print(command[0])
    process.setProgram(command[0])
    print(command[1:])
    process.setArguments(command[1:])
    process.start()

    while process.state() == QtCore.QProcess.Running:
        QtCore.QCoreApplication.processEvents()
        process.waitForReadyRead(20)

    print(process.readAllStandardOutput().data().decode())
    print(process.readAllStandardError().data().decode())
    print(process.errorString())
    print(process.exitStatus())
