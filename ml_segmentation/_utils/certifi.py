import os

def setup_certificates_mac():
    if os.name == 'posix':
        import certifi

        cacrt_path = certifi.core.where()
        os.environ['SSL_CERT_FILE'] = cacrt_path
        os.environ['REQUESTS_CA_BUNDLE'] = cacrt_path
