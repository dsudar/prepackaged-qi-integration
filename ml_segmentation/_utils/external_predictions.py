from pathlib import Path
import shutil

user_path = pathlib.Path.home() / 'Qi' / 'predictions'
prediction_path = "./output"

def load(specification = None):
    for filename in os.listdir(user_path):
        if specification is None or specification in filename:
            source_file = os.path.join(user_path, filename)
            destination = os.path.join(prediction_path, filename)
            if os.path.isfile(source_file):
                shutil.copy(source_file, destination)

def delete_predictions(specification, delete_All = False):
    for filename in os.listdir(user_path):
        if specification in filename or delete_All:
            os.remove(os.path.join(user_path, filename))
