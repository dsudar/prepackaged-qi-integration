from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
import logging
import sys


def init_logger():
    log_path = Path.home() / 'Qi' / 'logs'
    log_path.mkdir(parents=True, exist_ok=True)
    log_filename = log_path / f'logfile.log'

    logger = logging.getLogger()

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(message)s')

    # Set up rotating file handler
    if len(logger.handlers) == 0:
        file_handler = TimedRotatingFileHandler(filename=log_filename, when='h', interval=1, backupCount=10)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    # stream_handler = logging.StreamHandler(sys.stdout)
    # stream_handler.setFormatter(formatter)
    # logger.addHandler(stream_handler)

    logging.debug('init logger')
