import datetime
import logging
import os
from pathlib import Path
from typing import Literal, List

from PySide2 import QtCore

import numpy as np
from PyQiTissue import QiApi

from ml_segmentation._segmentation.QI.channel_data import get_np_roi_window, roi_to_bbox, \
    bbox_to_image_coords, channel_to_np, clip_bbox
from ml_segmentation._segmentation.QI.segmentation_data import create_segmentation, pad_labeled_image
from ml_segmentation._segmentation.models.exported_channel import ExportedChannel
from ml_segmentation._segmentation.models.segmentation_paths import SegmentationPaths
from ml_segmentation._ui.progress_dialog import show_progress_dialog
from ml_segmentation._utils.command import run_command
from ml_segmentation._utils.install_in_venv import activate_virtual_environment, get_venv_name


def run_segmentation(package: Literal['qi_stardist', 'qi_cellpose', 'qi_deepcell'],
                     channel_index: int, cyto_channel_index: int = -1,
                     cyto: Literal['None', 'donut', 'constrainedDonut'] = 'donut',
                     model: str = None, donut_width=None, gpu_mode='auto', extra_options: list[str] = [], roi=None,
                     combined_output=False):
    algo_name = package[3:]

    # show user progress
    dialog = show_progress_dialog(
        f"Running {algo_name.capitalize()} Segmentation, \n this may take a while...\n please don't close the application"
    )

    # create segmentation id from data
    dataset = QiApi.Dataset.currentDataset()

    dataset_name = QiApi.Dataset.title(dataset)
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")

    segmentation_id = f'{algo_name}-{model}-{channel_index}-{timestamp}'

    print(f'running segmentation {segmentation_id}')
    paths = init_paths()

    # export channels to np
    print(f'exporting channel data')
    exported_nuclear_channel = export_channel_data(dataset, channel_index, roi,
                                                   paths.input_path / 'exported_nuc_channel.npy')
    exported_cyto_channel = None
    if cyto_channel_index >= 0:
        exported_cyto_channel = export_channel_data(dataset, cyto_channel_index, roi,
                                                    paths.input_path / 'exported_cyto_channel.npy')

    result_path = (paths.predictions / (create_file_name(f'{dataset_name}-{segmentation_id}') + '.tiff')).as_posix()

    print('running segmentation, this may take a while.......')
    QtCore.QCoreApplication.processEvents()

    # run algo cli
    activate_virtual_environment(get_venv_name(package))

    command = [f'{algo_name}-cli', '-i', f'{exported_nuclear_channel.file_path.as_posix()}', '-o',
               f'{result_path}', '-g', gpu_mode]

    cyto_result_path = None
    if model is not None:
        command.extend(['-m', f'{model}'])
    if exported_cyto_channel is not None:
        command.extend(['-c', f'{exported_cyto_channel.file_path.as_posix()}'])
    if combined_output:
        cyto_result_path = (paths.predictions / (create_file_name(f'{dataset_name}-{segmentation_id}-cyto') + '.tiff')).as_posix()
        command.extend(['--output_cyto', f'{cyto_result_path}'])

    command.extend(extra_options)

    logging.debug(f'running segmentation command: {command}')
    run_command(command)
    if os.path.exists(result_path):
        logging.info(f'successfully ran segmentation command: {command}')
        logging.info(f'saved segmentation at {result_path}')
    else:
        dialog.close()
        print(f"error at running command: {command}\nlogs are at {(paths.user_home / 'logs')}")
        logging.error(f'commandline code: {command}\nfailed to create result file at {result_path}')
        return

    if roi is not None:
        logging.debug('padded output data for roi')
        pad_labeled_image(result_path, exported_nuclear_channel.roi_bbox,
                          exported_nuclear_channel.channel_np.shape)
        if cyto_result_path is not None:
            logging.debug(exported_cyto_channel.roi_bbox)
            pad_labeled_image(cyto_result_path, exported_cyto_channel.roi_bbox,
                              exported_cyto_channel.channel_np.shape)

    logging.debug('creating segmentations')
    create_segmentation(segmentation_id, result_path, cyto, donut_width, cyto_result_path)
    logging.info('created segmentations')

    dialog.close()


def init_paths() -> SegmentationPaths:
    user_path = Path.home() / 'Qi'
    user_path.mkdir(parents=True, exist_ok=True)
    input_path = user_path / 'input'
    input_path.mkdir(parents=True, exist_ok=True)
    prediction_path = user_path / 'predictions'
    prediction_path.mkdir(parents=True, exist_ok=True)

    return SegmentationPaths(user_path, input_path, prediction_path)


def export_channel_data(dataset, channel_index, roi, inputfile_name: Path) -> ExportedChannel:
    channel = QiApi.Channel.channels(dataset)[channel_index]
    channel_np = channel_to_np(channel)

    full_channel_shape = channel_np.shape
    clipped_bbox = [0, 0, channel_np.shape[0], channel_np.shape[1]]

    region_np = channel_np

    if roi is not None:
        print('extracting roi data')
        logging.debug('extracting roi data')
        roi_bbox = roi_to_bbox(roi)
        channel_bbox = bbox_to_image_coords(roi_bbox, channel)
        clipped_bbox = clip_bbox(channel_bbox, channel_np.shape)
        region_np = get_np_roi_window(clipped_bbox, channel_np)

        if region_np.size == 0:
            raise Exception('selected roi has no data')

    np.save(inputfile_name.as_posix(), region_np)
    logging.debug(f'roi data extracted and saved to {inputfile_name}')

    return ExportedChannel(channel_index, channel_np, clipped_bbox, inputfile_name)


def create_file_name(name: str) -> str:
    # replace some characters with _
    out = name.replace("(", "_").replace(")", "_")
    out = out.replace("-", "_")
    out = out.replace(" ", "_")
    # filter out non-alphanumeric
    out = "".join(x for x in out if x.isalnum() or x == '_')

    return out
