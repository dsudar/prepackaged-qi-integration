from pathlib import Path
from typing import Tuple

import numpy as np


class ExportedChannel:
    def __init__(self, index: int, channel_np: np.ndarray[int, int], roi_bbox: Tuple[int, int, int, int], file_path: Path):
        self.index = index
        self.channel_np = channel_np
        self.roi_bbox = roi_bbox
        self.file_path = file_path
