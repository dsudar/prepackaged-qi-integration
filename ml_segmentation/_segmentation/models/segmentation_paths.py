from pathlib import Path


class SegmentationPaths:
    def __init__(self, user_home: Path, input_path: Path, predictions: Path):
        self.user_home = user_home
        self.input_path = input_path
        self.predictions = predictions
