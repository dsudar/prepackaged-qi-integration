from typing import Tuple

from PyQiTissue import QiApi
import numpy as np


def channel_to_np(channel) -> np.ndarray[int, int]:
    # Get the buffer for the channel
    buffer_handle = QiApi.Channel.buffer(channel)
    buffer = memoryview(buffer_handle)

    if buffer.format == 'B':
        dtype = np.uint8
    elif buffer.format == 'H':
        dtype = np.uint16
    elif buffer.format == 'I':
        dtype = np.uint32
    else:
        raise Exception(f'Unknown format {buffer.format}')

    # Load the buffer data into a numpy array
    channel_np = np.frombuffer(buffer, dtype=dtype)
    channel_np = channel_np.reshape(buffer.shape)

    return channel_np


def roi_to_bbox(roi) -> Tuple[int, int, int, int]:
    """
    converts to bbox coordinates

    :param roi: the Qi Roi reference
    :return: 4 tuple of x0, y0, x1, y1 defining a rectangle in  roi scene coordinates
    """
    pos = QiApi.Annotation.position(roi)
    rect = QiApi.Annotation.rect(roi)

    # position needs to be offset with the rect coordinates
    x_coords = np.array([pos.x() + rect.left(), pos.x() + rect.right()]) \
        .astype(int)
    y_coords = np.array([pos.y() + rect.top(), pos.y() + rect.bottom()]) \
        .astype(int)

    return x_coords[0], y_coords[0], x_coords[1], y_coords[1]


def bbox_to_image_coords(bbox: Tuple[int, int, int, int], channel) -> Tuple[int, int, int, int]:
    offset: Tuple[int, int] = QiApi.Channel.channelProperties(channel)['registration']

    return \
        bbox[0] - offset[0], \
        bbox[1] - offset[1], \
        bbox[2] - offset[0], \
        bbox[3] - offset[1]


def clip_bbox(bbox: Tuple[int, int, int, int], channel_shape: Tuple[int, int]):
    return \
        np.clip(bbox[0], 0, channel_shape[1]), \
        np.clip(bbox[1], 0, channel_shape[0]), \
        np.clip(bbox[2], 0, channel_shape[1]), \
        np.clip(bbox[3], 0, channel_shape[0]), \



def get_np_roi_window(bbox: Tuple[int, int, int, int], channel_np: np.ndarray[int, int]):
    return channel_np[bbox[1]:bbox[3], bbox[0]:bbox[2]]


