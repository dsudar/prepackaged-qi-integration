from typing import Literal, Tuple
import logging
from PyQiTissue.QiApi import Feature
import os
import imageio.v2 as imageio
import numpy as np


def create_segmentation(name: str, labeled_image_path: str,
                        cyto: Literal['None', 'donut', 'constrainedDonut'] = 'donut', donut_width=None,
                        cyto_labeled_image_path: str = None):
    nuc_params = {
        "method": "importLabeledImage",
        "importImageLocation": os.path.abspath(labeled_image_path)
    }

    if cyto_labeled_image_path is not None:
        # combined output -> import cyto image as well
        cyto_params = {
            'active': True,
            "method": "importLabeledImage",
            "importImageLocation": os.path.abspath(cyto_labeled_image_path)
        }
    else:
        if cyto != 'None':
            if donut_width:
                cyto_params = {
                    'active': True,
                    'method': cyto,
                    'donutWidth': donut_width
                }
            else:
                cyto_params = {
                    'active': True,
                    'method': cyto
                }
        else:
            cyto_params = {
                'active': False,
            }

    segmentation = Feature.createSegmentation()

    Feature.setTitle(segmentation, name)
    Feature.setNucleusLabelingParameters(segmentation, nuc_params)
    Feature.setCytoplasmLabelingParameters(segmentation, cyto_params)
    Feature.startSegmentation(segmentation)


def pad_labeled_image(labeled_image_path: str, bbox: Tuple[int, int, int, int], buffer_shape):
    labeled_image = imageio.imread(labeled_image_path)

    logging.debug(f'padding image of shape {labeled_image.shape} to shape {buffer_shape}. bbox: {bbox}')

    padded_array = np.zeros(buffer_shape, dtype=labeled_image.dtype)

    padded_array[bbox[1]:bbox[3], bbox[0]:bbox[2]] = labeled_image

    # Save the padded array as a TIFF file
    imageio.imwrite(labeled_image_path, padded_array)
