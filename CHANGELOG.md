# [](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.4.1...v) (2023-11-23)



## [1.4.1](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.4.0...v1.4.1) (2023-11-23)


### Bug Fixes

* **install-in-venv:** disable pip progress bar to declutter logfile ([2e0ad09](https://gitlab.com/claiv/prepackaged-qi-integration/commit/2e0ad098e28c4d63ab7b982111869fd7125eed8a))



# [1.4.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.3.0...v1.4.0) (2023-11-21)


### Bug Fixes

* **command:** remove text=True ([0cd66d8](https://gitlab.com/claiv/prepackaged-qi-integration/commit/0cd66d8aa269e9ea7e582a0a4237b3668c604e36))
* **logging:** logs in hex if for some reason ascii errors restrict logging ([0b99f6b](https://gitlab.com/claiv/prepackaged-qi-integration/commit/0b99f6b7c6558b933c3af0b07129baeb44f1ac75))
* **sub-processes:** fixed encoding error ([fdeaa45](https://gitlab.com/claiv/prepackaged-qi-integration/commit/fdeaa4526d753c717b1844a710a2243a2072c6fd))
* **ui:** move gpu option above run button ([b7724f9](https://gitlab.com/claiv/prepackaged-qi-integration/commit/b7724f9e810b3a4ca824e80b4ec823a299fab00a))


### Features

* **gpu:** Adds option to not use gpu ([68e22de](https://gitlab.com/claiv/prepackaged-qi-integration/commit/68e22def39109f6b8802acb283eba99f744d35db))



# [1.3.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.2.0...v1.3.0) (2023-10-30)


### Bug Fixes

* **command:** explicit utf-8 encoding to avoid decoding errors ([9a89869](https://gitlab.com/claiv/prepackaged-qi-integration/commit/9a8986932ced623d3fde0355a195966a437c1fa4))
* **command:** universal newlines to avoid decoding errors ([c0e0365](https://gitlab.com/claiv/prepackaged-qi-integration/commit/c0e0365501378014d130719a6212695df1163cf5))
* **deepcell:** float datatype for mpp, imports ([a6af98c](https://gitlab.com/claiv/prepackaged-qi-integration/commit/a6af98c5b6e10b7791922b866a9a30cd2feebf2a))
* **dependency:** prepacked-cellpose 3.1.1: with removed explicit +cu118 as it is not available for all platforms ([02188a3](https://gitlab.com/claiv/prepackaged-qi-integration/commit/02188a336594ce1cd0ff90f025a4a872ba8f7a03))
* **install-cellpose:** install in own function avoiding ipython eof error ([84f9b0b](https://gitlab.com/claiv/prepackaged-qi-integration/commit/84f9b0ba377a1614dc439b31a4aded392ff018a1))
* **install:** encapsulate install to avoid ipython eof error ([56be461](https://gitlab.com/claiv/prepackaged-qi-integration/commit/56be4616ed721cb4424a6d943fa8436d6f5106ef))


### Features

* **deepcell:** adds option for selecting deepcells segmentation parameter image microns per pixel ([90b0b99](https://gitlab.com/claiv/prepackaged-qi-integration/commit/90b0b99fc082573e490b6de6ab4e38d434599cf0))



# [1.2.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.1.0...v1.2.0) (2023-10-06)


### Bug Fixes

* **dependency:** prepacked_deepcell 0.5.4 with pinned deepcell version before authentication requirement ([6dbc668](https://gitlab.com/claiv/prepackaged-qi-integration/commit/6dbc668f59a4335f34b234739c7ff0ad0889adad))
* **run-deepcell:** init logger ([7fe8aeb](https://gitlab.com/claiv/prepackaged-qi-integration/commit/7fe8aeb5e58739ab55493bc3ea9e630cf2966ab4))
* **runner:** fix cutting out roi window from channels ([dc74354](https://gitlab.com/claiv/prepackaged-qi-integration/commit/dc74354c7408b7b8c24d9c68af053632bc90b284))
* **segmentation:** fixes logging error when predicting whole picture ([506a989](https://gitlab.com/claiv/prepackaged-qi-integration/commit/506a98919a53d0f2ae801e608d120f80488401f9))
* **ui:** fix disabling of cyto options depending on model ([fad423f](https://gitlab.com/claiv/prepackaged-qi-integration/commit/fad423ffb65ca14ae176adf73f07f9b2123a8625))


### Features

* **deepcell:** add mesmer-both deepcell model, passing it as combined-output model to runner ([71157e2](https://gitlab.com/claiv/prepackaged-qi-integration/commit/71157e2e00a1ce8ab1058a57d0e4579aa6159fcc))
* **runner:** allow for cyto channel export + passing it to the cli ([9ba4f5a](https://gitlab.com/claiv/prepackaged-qi-integration/commit/9ba4f5ab18b3d30e79a91eb90043cc4323e4673d))
* **runner:** implement nuclear + cyto combined output: use cyto output from algorithm for cyto segmentation ([b0ea665](https://gitlab.com/claiv/prepackaged-qi-integration/commit/b0ea6656a3f0a8b444b0157134a43a375f8d99c8))
* **segmentation-data:** also importing cyto labeled image ([195d057](https://gitlab.com/claiv/prepackaged-qi-integration/commit/195d057c5b3a0624055e21490f67bf054b54df85))
* **ui:** add cyto channel selection that can be enabled when needed ([3748fc3](https://gitlab.com/claiv/prepackaged-qi-integration/commit/3748fc3e0a9190168f5c3f2103fbac82d6846910))



# [1.1.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v1.0.0...v1.1.0) (2023-09-27)


### Bug Fixes

* **custom-model:** checking if custom model is selected before adding the appropriate cli option ([05ab8f2](https://gitlab.com/claiv/prepackaged-qi-integration/commit/05ab8f28dd8d72b12cc339a1f08049914b37f816))
* **logging:** avoid adding handler multiple times, fix logs being written multiple times ([178bcad](https://gitlab.com/claiv/prepackaged-qi-integration/commit/178bcad4cc95e50e67f5f533b42d4277ba048f5f))
* **logging:** reported wrong save file place ([ab41c04](https://gitlab.com/claiv/prepackaged-qi-integration/commit/ab41c046d0c0646d493a019606e7e26fea07378c))
* **ui:** import QtWidgets ([e9cccd6](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e9cccd66e2379905f039c615f1be917bf20a761d))


### Features

* **cellpose-gpu:** add extra-index-url for cuda torch download, update matrix ([9f36a9d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/9f36a9d83e4efbaa3d4e31452ac28a1786a6f2fe))
* **deepcell:** add basic ui + running deepcell ([69ecc22](https://gitlab.com/claiv/prepackaged-qi-integration/commit/69ecc22e5598a443f82e214bcfd22b5dfd5a2aaa))
* **install-in-venv:** add capacity to use multiple extra-index-urls ([ccde256](https://gitlab.com/claiv/prepackaged-qi-integration/commit/ccde2567ba080a50170bad380673c09a48cd7370))
* **segmentation:** Added parameter donut width to segmentation ([8761252](https://gitlab.com/claiv/prepackaged-qi-integration/commit/876125221118da32fb43287a88008a464f1d4587))
* **ui:** Added param donut width ([1c3a75b](https://gitlab.com/claiv/prepackaged-qi-integration/commit/1c3a75ba9bb2fd3d3d7dd2fa8aaca32b3119b993))



# [1.0.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.4.1...v1.0.0) (2023-08-20)


### Bug Fixes

* **button not working:**   ([e391449](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e3914492834b2deb99745090d716a8ef1a676e09))
* **changed logfile path to user dir:** changed logfile path to user dir / Qi / logs ([a4efce6](https://gitlab.com/claiv/prepackaged-qi-integration/commit/a4efce63f175cdc50ccb71e9f4e03a2f053ed707))
* **installation errror:** virtual env was not corectly installed in user/Qi path ([7c9159d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/7c9159df2edbf6f17fbd6ccfd98fafc7da6ba447))
* **load from user dir:** Now loads the predicted data from user directory ([db47886](https://gitlab.com/claiv/prepackaged-qi-integration/commit/db478864d1508b216c5fecf9c6e67187af6caca8))
* **redirected traffic:** redirected all in and file output to user dir ([f8e35fb](https://gitlab.com/claiv/prepackaged-qi-integration/commit/f8e35fbe2edebde7477aa3154fc43b227545ae89))
* **virtual env now in user folder:** Changed the directroy of the vitual env to the user/qi folder ([8ad32bb](https://gitlab.com/claiv/prepackaged-qi-integration/commit/8ad32bb2a13e6c915243e5e40aa3d98f6d7dab8d))
* **wrong dot:** mistyped dot  ([bb2c1f9](https://gitlab.com/claiv/prepackaged-qi-integration/commit/bb2c1f91f707a5b63d347b91d1085e0654dd3f24))


### Features

* **cellpose:** custom model disable auto diameter (not available to custom models) some refactoring ([e8b422e](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e8b422ed0cd8aa8f597f7f6249ef611ebeb3cdbc))
* **logging:** also logging process output ([c5ae6e8](https://gitlab.com/claiv/prepackaged-qi-integration/commit/c5ae6e81d0ee46016834ea7ec090eadc7f29e494))
* Saves predictions additionally in user dir ([afcf27d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/afcf27da0ab0c442c883a42b31651fd6cc16754d))
* **stardist:** add ui and implement using a custom model from a folder ([42108bd](https://gitlab.com/claiv/prepackaged-qi-integration/commit/42108bd42fa2906a3ffc7f17325d6460a5a982cd))



## [0.4.1](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.4.0...v0.4.1) (2023-07-14)



# [0.4.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.3.1...v0.4.0) (2023-07-14)


### Features

* **install:** pinning versions of algorithms in install scripts ([53db14d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/53db14dc185d2312eeea3673a01adc1d4db9a2f5))



## [0.3.1](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.3.0...v0.3.1) (2023-07-13)


### Bug Fixes

* **channel-export:** converting the roi rect coordinates to 'image space' before using to extract the numpy data ([2cdef76](https://gitlab.com/claiv/prepackaged-qi-integration/commit/2cdef76c1cc779c6d0dc839d716f4d1cc87af0c0))



# [0.3.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.2.2...v0.3.0) (2023-07-06)


### Bug Fixes

* **channel-export:** checking buffer format and choosing np data type accordingly ([87df41b](https://gitlab.com/claiv/prepackaged-qi-integration/commit/87df41ba1224ea455fd69e7cdbd4050b21ddba27))


### Features

* **cellpose:** add average diameter option ([8741201](https://gitlab.com/claiv/prepackaged-qi-integration/commit/8741201aece5d99f5fd61bd6ef5f8a366b225f1c))
* **cellpose:** add flow and cellprob threshold options ([96ed699](https://gitlab.com/claiv/prepackaged-qi-integration/commit/96ed69948a88779485ce1e53ec1e42746899ec7f))
* **cellpose:** add net avg option ([03c8bf3](https://gitlab.com/claiv/prepackaged-qi-integration/commit/03c8bf30da1b4edc70864068f5d7231aaa956c19))
* **roi:** add option to run algos on all ROIs ([0361441](https://gitlab.com/claiv/prepackaged-qi-integration/commit/0361441c17ea587edd61fffa535d3a8f976da6a1))



## [0.2.2](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.2.1...v0.2.2) (2023-06-28)


### Bug Fixes

* **segmentation:** cleaning up output file name, so it contains alphanumeric characters or hyphens ([e0e011c](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e0e011c339648a40185184d586eba25050e6e28a))



## [0.2.1](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.2.0...v0.2.1) (2023-06-27)


### Bug Fixes

* **command:** not showing empty cmd window on Windows ([7c77b43](https://gitlab.com/claiv/prepackaged-qi-integration/commit/7c77b436eb5b202503808b4b71e4ddca0b2161e7))
* **install-cellpose:** fix dialog text saying Cellpose now ([1e832fd](https://gitlab.com/claiv/prepackaged-qi-integration/commit/1e832fd5e8b5ca8c6196cb6e07b1b17362967d7f))



# [0.2.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/0.1.0...v0.2.0) (2023-06-27)


### Features

* **progress_progress:** add progress dialog while installing and segmenting ([cbd0570](https://gitlab.com/claiv/prepackaged-qi-integration/commit/cbd0570c43aa2b34135238ed6c72c68256974db6))



# [0.1.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/v0.1.0...0.1.0) (2023-06-26)



# [0.1.0](https://gitlab.com/claiv/prepackaged-qi-integration/compare/adc61e2bb9ebfcfeead0a4532cd0edfbca242ca5...v0.1.0) (2023-06-26)


### Bug Fixes

* path for unix like python xecutable ([e47512f](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e47512f6ad719d79fdf3778c5d675d2277361f23))
* using normal subproces again ([525a4b5](https://gitlab.com/claiv/prepackaged-qi-integration/commit/525a4b5f15a5efe50117eddf36638f0e0b7b1ba0))


### Features

* add channel list in widget instead of channel index ([e15e424](https://gitlab.com/claiv/prepackaged-qi-integration/commit/e15e4243786b7fbf8f36eac9c515c39b5d9d4e7e))
* add code to create venv and install packages into it, add script to install stardist in its own venv ([adc61e2](https://gitlab.com/claiv/prepackaged-qi-integration/commit/adc61e2bb9ebfcfeead0a4532cd0edfbca242ca5))
* add run_command_qprocess for async integraiont with qt ([0a0b6b6](https://gitlab.com/claiv/prepackaged-qi-integration/commit/0a0b6b6fb64ca95c54ab116f50b8931ff377bb03))
* add script to activate environement and run stardist, refactoring ([f4f1c6d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/f4f1c6d8d61ec75bc994b5fff82322e20e33d068))
* add widget to run Stardist, using it in run_stardist ([fffa49d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/fffa49d83068120ecb82f33d03ff3343b662d20c))
* **cellpose:** add basic cellpose widget and execution ([d55342c](https://gitlab.com/claiv/prepackaged-qi-integration/commit/d55342c5dd32cebacd1c633c9cc2a7a74426e2d2))
* **command:** new live process output ([b8272ca](https://gitlab.com/claiv/prepackaged-qi-integration/commit/b8272ca60c2a2fd216da1db24110ff164f708b24))
* **command:** read output line by line ([cc7d864](https://gitlab.com/claiv/prepackaged-qi-integration/commit/cc7d8641c0e6196ad5d2e608fa99b42ace16ed4f))
* printing the pip output, refactor: refactor create and install methods ([6791d4d](https://gitlab.com/claiv/prepackaged-qi-integration/commit/6791d4d001f95ef10c7c41593d7e7cf606b8f418))
* **roi:** implement only extracting the data in a roi and then padding it accordingly for re-import ([7c3cc96](https://gitlab.com/claiv/prepackaged-qi-integration/commit/7c3cc9693b084e81771015c499c07950cdde01b8))
* **round-trip:** automatically re-importing the labeled mask and creating the segmentation, automatically generating tiff filenames ([1e6975c](https://gitlab.com/claiv/prepackaged-qi-integration/commit/1e6975cf6c1966e8fe88326661fac7044a6ef5d4))
* **run:** add selection for cytoplasm segmentation, choose none, donut or constrained donut ([991a652](https://gitlab.com/claiv/prepackaged-qi-integration/commit/991a652c81fd1a3da35ee5cf9ea6849cb3e34231))
* **ui:** add roi selection to ui ([1c5bc54](https://gitlab.com/claiv/prepackaged-qi-integration/commit/1c5bc5465b36c137a4f3982991844f66d9818f0e))
* **ui:** closing dialog when finished ([318956f](https://gitlab.com/claiv/prepackaged-qi-integration/commit/318956f254b3e26bea5e7715e87b65e2aaaf2dcf))
* **ui:** select first roi by default if available ([5c2370b](https://gitlab.com/claiv/prepackaged-qi-integration/commit/5c2370be06920f5049b8f1b1bc77411d41a55123))



