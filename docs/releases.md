# Releases
To make a release a bit of manual work is currently needed:
- install semantic release `pip install python-semantic-release`
- update version + tag `python -m semantic_release version`
- update changelog `conventional-changelog -p angular -i CHANGELOG.md -s -r 0` + commit
  - conventional changelog is a npm package and can be installed via npm (`npm install -g conventional-changelog-cli`)
- commit changelog
- push commit + tags `git push --follow-tags`
- create release from tag in gitlab. copy changes from changelog for the version