
# Usage
The scripts are designed to be run with QiTissue: 
- Download the project
  - choose source code (zip) from [Releases](https://gitlab.com/claiv/prepackaged-qi-integration/-/releases)
  - unpack the zip archive somewhere
- In QiTissue: Edit -> Preferences -> Python
  - Configure a new path to search for Python scripts
  - set the path to the unpacked project (i.e. prepackaged-qi-integration)

![Configure a new path to search for Python scripts](./docs/images/preferences-scrips.png)
- restart QiTissue
- the scripts are available under Python->prepackaged_integration in the top bar

![Python Menu](./docs/images/python-menu.png)
- the important scripts are `install_stardist` and `run_stardist`
- first run `install_stardist`
  - creates a virtual environment and installs our prepackaged stardist package
- then it should be possible to run `run_stardist`
  - creates a small dialog to choose the model and the channel
  - runs the algorithm and imports the segmentation mask

![Stardist Dialog](./docs/images/stardist-dialog.png)

The same goes for the other algorithms

# Requirements
## Windows
Some of the Algorithms will need long path names enabled: [link](https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=powershell#enable-long-paths-in-windows-10-version-1607-and-later)